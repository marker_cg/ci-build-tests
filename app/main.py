import os
from db_utils import Mysql
import time
import pymysql
from dotenv import load_dotenv

load_dotenv()


# Create custom mysql class
class CustomMysql(Mysql):
    def __init__(self):
        # db_host = os.getenv("DB_HOST")
        # db_user = os.getenv("DB_USER")
        # db_password = os.getenv("DB_USER_PASSWORD")
        # db_database = os.getenv("DB_DATABASE")

        # How to protect sensitive data in python for gitlab ci ????! Bed code below
        super().__init__(host='mysql', user='python_app_user', password='password', database='python_shop')

    def create_shop(self):
        create_table_query = """
            CREATE TABLE shop (
                id INT AUTO_INCREMENT PRIMARY KEY,
                item VARCHAR(255) NOT NULL,
                price FLOAT NOT NULL
            )
        """
        self.execute_query(create_table_query)

    def add_item(self, item, price):
        add_item_query = "INSERT INTO shop (item, price) VALUES (%s, %s)"
        self.execute_query(add_item_query, (item, price))

    def delete_item(self, item):
        delete_item_query = "DELETE FROM shop WHERE item = %s"
        self.execute_query(delete_item_query, (item,))

    def delete_shop(self):
        delete_table_query = "DROP TABLE shop"
        self.execute_query(delete_table_query)

