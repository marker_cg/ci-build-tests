# Lesson-31. GitLab CI
This project demonstrates the usage of GitLab CI/CD for automated build, testing, and deployment workflows. The main workflow is defined in the **gitlab-ci.yml file**. It is triggered on every push to the main branch.


## Stages

1. **Build**: Compiles the application and builds Docker images.
2. **Test**: Runs tests in an isolated environment using Docker.
3. **Deploy**: Deploys the application using Docker images and create gitlab page for coverage report.


## gitlab-ci.yml

```sh
nstages:
  - build
  - test
  - deploy


services:
  - name: docker:20.10.16-dind
    alias: docker

Build:
  image: docker:20.10.16
  stage: build
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker compose build python_app 
    - docker tag python_app:latest $CI_REGISTRY_IMAGE/python_app:latest
    - docker push $CI_REGISTRY_IMAGE/python_app:latest
    - docker compose build app-coverage
    - docker tag app-coverage:latest $CI_REGISTRY_IMAGE/app-coverage:latest
    - docker push $CI_REGISTRY_IMAGE/app-coverage:latest
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'

Tests:
  image: docker:20.10.16
  stage: test
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE/app-coverage:latest
    - docker compose up -d mysql
    - |
      echo "Waiting for MySQL to become healthy..."
      for i in {1..10}; do
        if [ "$(docker inspect --format='{{.State.Health.Status}}' mysql-server)" == "healthy" ]; then
          echo "MySQL is healthy. Continuing..."
          break
        else
          echo "Waiting for MySQL to become healthy..."
          sleep 30
        fi
      done
      if [ $i -eq 10 ]; then
        echo "MySQL did not become healthy. Exiting..."
        exit 1
      fi
    - docker compose up app-coverage
    - docker cp app-coverage:/app/coverage/ ./public/
  artifacts:
    paths:
      - coverage/
    expire_in: "30 days"
    when: always
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'

Deploy:
  image: docker:20.10.16
  stage: deploy
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE/python_app:latest
    - docker tag $CI_REGISTRY_IMAGE/python_app:latest $CI_REGISTRY_IMAGE:latest
    - docker push $CI_REGISTRY_IMAGE:latest
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'


pages:
  image: alpine:latest
  stage: deploy
  script:
    - mv coverage/ public/
  artifacts:
    paths:
      - public
  dependencies:
    - Tests
  only:
    - main

```

## Usage
To use this workflow, follow these steps:

1. Configure your GitLab project with the necessary CI/CD variables (`DB_HOST`, `DB_USER`, `DB_USER_PASSWORD`, etc.).
2. Push code to your repository's `main` branch.
3. The pipeline will automatically build, test, and deploy your application and create gitlab page.(for example: https://ci-build-tests-marker-cg-edcd900c1f4199b5cb361d3d1ff52f766508d2.gitlab.io/)



## Result
<img src="https://i.imgur.com/OVuKbkD.png" alt="drawing" width="95%"/></img>
_____________________________________________________
> **Note:** Ensure that your GitLab Runner has Docker and Docker Compose installed and is configured for DinD service.








